# DEMO - JMeter Load testing with GitLab CI

此 DEMO 的相關文章，請參閱 - 「[透過 GitLab CI + K6 進行小規模 Load Testing](https://chengweichen.com//2022/02/gitlab-ci-load-testing-k6.html)」。

## 使用方式

1. 直接 clone / fork 此 Project。
2. 進入 `CI/CD > Pipelines`，並按下 `Run Pipeline`。
3. 根據你想要實施 Load testing 的目標對象，輸入以下 Variables：

```
  K6_VUS = 要模擬多少人數
  K6_DURATION = 測試總共要跑多久時間
  TESTING_URL = 施測的目標網址
  TESTING_KEYWORD: 目標網址的 Response 必須包含哪個字串
```

4. 最後，執行 CI Job 即可。
